from ims_base.serializers import BaseModelSerializer
from rest_framework import serializers


class BatchSerializer(BaseModelSerializer):
    def validate(self, attrs):
        validated_data = super().validate(attrs)
        if (
            self.instance
            and not self.context["request"]
            .user.groups.filter(name__in=["Admin", "Manager"])
            .exists()
        ):
            if self.instance.status in ["cr", "co"] or validated_data["status"] == "co":
                raise serializers.ValidationError(
                    {"status": "Operation not permitted for this user"}
                )
        return validated_data

from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from .apis import BatchSubject

urlpatterns = [
    path("subject/<int:batch_id>/", BatchSubject.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns, allowed=["json"])

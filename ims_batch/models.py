from django.db import models
from django.db.models.base import post_save
from django.dispatch import receiver
from ims_base.models import AbstractBaseDetail
from ims_notification.services import add_group_notification
from reusable_models import get_model_from_string


class Batch(AbstractBaseDetail):
    BATCH_STATUS = [
        ("ns", "not started"),
        ("st", "started"),
        ("pa", "paused"),
        ("cr", "completion requested"),
        ("co", "completed"),
    ]
    is_registration_open = models.BooleanField(default=True)
    course = models.ForeignKey(
        get_model_from_string("COURSE"), on_delete=models.CASCADE
    )
    batch_mentor = models.ForeignKey(
        get_model_from_string("FACULTY"),
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    status = models.CharField(max_length=2, choices=BATCH_STATUS, default="st")
    room = models.ForeignKey(
        get_model_from_string("ROOM"), on_delete=models.CASCADE, null=True, blank=True
    )


@receiver(post_save, sender=Batch)
def send_overdue_extension_notification(instance, created, **kwargs):
    if instance.status == "cr":
        notification = f"{instance} batch is requested for completion."
        add_group_notification(notification, ["Admin", "Manager"])

from django.db.models import ObjectDoesNotExist
from ims_base.apis import BaseAPIView, BaseAPIViewSet
from ims_user.apis import UserFilter
from rest_framework.exceptions import NotFound
from rest_framework.response import Response
from reusable_models import get_model_from_string

from .models import Batch

Subject = get_model_from_string("SUBJECT")
Course = get_model_from_string("COURSE")


class BatchSubject(BaseAPIView):
    def get(self, request, **kwargs):
        batch_id = kwargs.get("batch_id")
        try:
            course = Batch.objects.get(pk=batch_id).course
        except ObjectDoesNotExist:
            raise NotFound("Batch does not exist.")
        else:
            return Response(course.subjects.all().values())


class BatchAPI(BaseAPIViewSet):
    filter_backends = BaseAPIViewSet.filter_backends + [
        UserFilter,
    ]
    obj_user_groups = ["Admin", "Manager"]
    user_lookup_field = "batch_mentor__staff__user"

from django.apps import AppConfig


class IMSBatchConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ims_batch"

    model_strings = {
        "BATCH": "Batch",
    }
